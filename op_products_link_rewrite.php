<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Op_products_link_rewrite extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'op_products_link_rewrite';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'OrangePix';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Repair the product URL');
        $this->description = $this->l('Rewrite the product URL');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {

        return parent::install() &&

            $this->registerHook('backOfficeHeader');
    }

    public function uninstall()
    {

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        if (Tools::isSubmit('form-submit-repair')==true){
            $this->context->smarty->assign('products', $this->UrlFix());
        }

        $this->context->smarty->assign('module_dir', $this->_path);
        $token = Tools::getAdminTokenLite('AdminModules');
        $action_form = $this->context->link->getAdminLink('AdminModules', false)
          .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&token='.$token;
        $this->context->smarty->assign('action_form', $action_form);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output;
    }


    public function UrlFix(){

        $products = Product::getSimpleProducts($this->context->language->id,$this->context);
        $repairs = array();
        foreach ($products as $prod) {
            $product = new Product($prod['id_product']);
            $this->LinkRewrite($product);
            $repairs[$product->id]['id']= $product->id;
            if (version_compare(_PS_VERSION_, '1.6.99.99', '<')) {
                $url = $this->context->link->getAdminLink('AdminProducts', true)
                  . '&id_product=' . $product->id . '&updateproduct';
            } else {
                $url = $this->context->link->getAdminLink(
                  'AdminProducts',
                  true,
                  array('updateproduct' => true, 'id_product' => $product->id)
                );
            }
            $repairs[$product->id]['url']= $url;
            $repairs[$product->id]['name']= $product->name;

            $repairs[$product->id]['link_rewrite']= $product->link_rewrite[$this->context->language->id];
            $product->save();
        }
        return $repairs;

    }

    /*
        Create the link rewrite if not exists or invalid on product creation
    */
    public function LinkRewrite(&$product)
    {
        foreach ($product->name as $id_lang => $name) {
            $product->link_rewrite[$id_lang] = Tools::link_rewrite($name);
        }

        return true;
    }



    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {

    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {

    }

}
