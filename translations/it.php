<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{op_products_link_rewrite}prestashop>op_products_link_rewrite_2c7fa61fee9287f8f58a263e271bbe21'] = 'Ripara l\'URL del prodotto';
$_MODULE['<{op_products_link_rewrite}prestashop>op_products_link_rewrite_dbcf57a38141cef2406fa5564b6c1f6a'] = 'Riscrivi l\'URL del prodotto';
$_MODULE['<{op_products_link_rewrite}prestashop>configure_2c7fa61fee9287f8f58a263e271bbe21'] = 'Ripara l\'URL del prodotto';
$_MODULE['<{op_products_link_rewrite}prestashop>configure_0493d27e20d11e356e53e0a730b0ad08'] = 'Riparazione';
$_MODULE['<{op_products_link_rewrite}prestashop>configure_67ecc442e2f898c999cede1d70db2ac4'] = 'Prodotti riparati';
$_MODULE['<{op_products_link_rewrite}prestashop>configure_b03ca77f349c4bee120076e71c835830'] = 'ID prodotto:';
$_MODULE['<{op_products_link_rewrite}prestashop>configure_80d4e11e891463ae328b0757147cfe2a'] = 'link-rewrite';
