{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<!------ Include the above in your HEAD tag ---------->
<div class="panel">
	<h3><i class="icon icon-link"></i> {l s='Repair the product URL' mod='op_products_link_rewrite'}</h3>
	<div class="mb-5">
		<form class="col" action="{$action_form}" method="post">
			<input type="hidden" name="form-submit-repair" value="1">
			<button type="submit" class="btn btn-primary pull-right">{l s='Repair' mod='op_products_link_rewrite'}</button>
		</form>
	</div>
{if count($products)>0}
	<div>
		<h3 class="col-1"><i class="icon icon-list-ul mr-1"></i>{l s='Repaired Products' mod='op_products_link_rewrite'}:</h3>
	</div>
	{foreach from=$products item=product}
		<div class="m-1 border-bottom">
			<span class="mr-3">{l s='Product id:' mod='op_products_link_rewrite'}</span>({$product.id})<label class="ml-3 mr-3">{l s='link-rewrite' mod='op_products_link_rewrite'}</label><span class="mr-2">=></span><a href="{$product.url}" target="_blank">{$product.link_rewrite}</a><br/>
		</div>
	{/foreach}
{/if}
</div>


